use crate::{color_to_gl, config, draw_rect, draw_text, FileManager, Program, Vertex};
use glium::Surface;
use lazy_static::lazy_static;
use ropey::Rope;
use sana::{Sana, Spanned};
use serde::Deserialize;
use std::collections::{BTreeMap, HashMap};
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::stdout;
use std::io::BufReader;
use std::io::BufWriter;
use std::path::Path;

#[cfg(feature = "highlighting")]
#[derive(Debug, Clone, Copy, PartialEq, Sana)]
#[backend(rust)]
enum Rust {
    #[regex("\".*\"")]
    String,
    #[regex("//.*")]
    Comment,
    #[regex(r#"\w+\("#)]
    Function,
    #[regex(r#"'.'"#)]
    Char,
    #[regex(r#"(<|>|&|\+|-|\*|/|=|!)"#)]
    Operator,
    #[regex(r#"(String|str|static|ref|mut|char|bool|(i|u|f)(128|64|32|16|8)) "#)]
    Type,
    #[regex(r#"(let|fn|impl|use|struct|enum) "#)]
    Keyword,
    #[regex(r#"\w+::"#)]
    Namespace,
    #[error]
    Error,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub relative_lines: bool,
    pub shell: String,
    pub font_size: f32,
    pub colors: BTreeMap<String, [u8; 3]>,
    pub highlighting: ConfigColor,
}

#[derive(Deserialize, Debug)]
pub struct ConfigColor {
    pub background: String,
    pub foreground: String,
    pub line_numbers: String,
    pub keywords: String,
    pub functions: String,
}

struct StyleMap {
    style: Style,
    line: i64,
    pos: i64,
}

#[derive(Clone, Debug)]
pub enum Mode {
    Normal,
    Insert,
}

struct Style {
    color: [u8; 3],
    bold: bool,
    italic: bool,
}

enum EditType {
    Insert,
    Delete,
}

struct Edit<'a> {
    pos: i64,
    content: &'a str,
    _type: EditType,
}

pub struct Editor {
    x: i64,
    y: i64,
    command: Vec<char>,
    cursor_y: i32,
    longer_lines: i32,
    mode: Mode,
    filename: String,
    rope: Rope,
    clipboard: String,
    line_index: HashMap<i32, i32>,
    screen_dimensions: (i32, i32),
    edits: Vec<Edit<'static>>,
}

impl Editor {
    pub fn new(filename: &str) -> Self {
        let path = Path::new("./").join(filename);
        let reader = BufReader::new(
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(&path)
                .unwrap(),
        );
        let mut rope = Rope::from_reader(reader).expect("Rope could not be built");
        if rope.len_chars() == 0 {
            rope.insert_char(0, ' ');
        }
        let line_index = HashMap::new();
        Self {
            x: 0,
            y: 0,
            cursor_y: 0,
            command: vec![],
            longer_lines: 0,
            mode: Mode::Normal,
            filename: filename.to_owned(),
            rope,
            clipboard: String::new(),
            screen_dimensions: (0, 0),
            line_index,
            edits: vec![],
        }
    }
    pub fn up(&mut self, rows: u16) {
        let mut rows = 1;
        //if self.rope.line((self.cur_y() - 1)).len_chars() > self.grid_width() as usize {
        //let longer_lines = self.rope.line(self.cur_y()).len_chars() as i32 / self.grid_width();
        //self.longer_lines -= longer_lines;
        //rows += longer_lines;
        //}
        if self.cursor_y - (rows as i32) < 0 {
            self.move_view((self.cursor_y as i64 + rows as i64) * -1);
            self.cursor_y = 0;
        } else {
            self.cursor_y -= rows as i32;
        }
    }
    pub fn down(&mut self, rows: u32) {
        let mut rows = 1;
        //if self.rope.line(self.cur_line()).len_chars() > self.grid_width() as usize {
        //self.longer_lines += self.rope.line(self.cur_line()).len_chars() as i32 / self.grid_width();
        //rows += 1;
        //}
        if self.cursor_y as u32 + rows > self.grid_height() as u32 - 4 {
            self.cursor_y = self.grid_height() - 4;
            self.move_view(self.cursor_y as i64 + rows as i64 - (self.grid_height() - 4) as i64);
        } else {
            self.cursor_y += rows as i32;
        }
    }
    pub fn left(&mut self, columns: u16) {
        if self.x > 0 {
            self.x -= columns as i64;
        } else if self.rope.line(self.cur_y()).len_chars() > self.grid_width() as usize {
            self.x = self.grid_width().into();
            self.cursor_y -= 1;
        }
    }
    pub fn right(&mut self, columns: u16) {
        self.x += columns as i64;
        if self.x >= self.grid_width() as i64 {
            self.x = 0;
            self.cursor_y += 1;
        }
    }
    pub fn move_view(&mut self, lines: i64) {
        if self.y as i64 + lines >= 0 {
            self.y += lines;
        } else {
            self.y = 0;
        }
    }
    pub fn center(&mut self) {
        if self.cursor_y > (self.grid_height() / 2) {
            self.y += self.cursor_y as i64 - (self.grid_height() as i64 / 2);
        }
    }
    pub fn cur_y(&self) -> usize {
        self.y as usize + self.cursor_y as usize
    }
    pub fn cur_line(&self) -> usize {
        self.line_index
            .get(&(self.cur_y() as i32))
            .unwrap_or(&0)
            .to_owned() as usize
    }
    pub fn get_current_pos(&self) -> usize {
        self.rope.line_to_char(self.cur_y()) + self.x as usize
    }
    pub fn insert(&mut self, pos: usize, string: &str) {
        self.rope.insert(pos, string);
    }
    pub fn remove(&mut self, range: std::ops::Range<usize>) {
        self.rope.remove(range);
    }
    pub fn grid_height(&self) -> i32 {
        (self.screen_dimensions.1 as f32 / config.font_size) as i32
    }
    pub fn grid_width(&self) -> i32 {
        (self.screen_dimensions.0 as f32 / (config.font_size / 2.0)) as i32
    }
    fn draw_text<'a>(
        &self,
        gui: &mut crate::GuiState,
        target: &mut glium::Frame,
        cache: &mut rusttype::gpu_cache::Cache<'a>,
    ) {
        let (screen_width, screen_height): (f32, f32) =
            gui.display.gl_window().window().inner_size().into();
        let down = (screen_height / config.font_size) as usize + self.y as usize - 3;
        if down >= self.rope.len_lines() {
            self.draw_text_ranged(self.y as usize..self.rope.len_lines(), gui, target, cache);
        } else {
            self.draw_text_ranged(self.y as usize..down, gui, target, cache);
        }
    }
    fn draw_text_ranged<'a>(
        &self,
        range: std::ops::Range<usize>,
        gui: &mut crate::GuiState,
        target: &mut glium::Frame,
        cache: &mut rusttype::gpu_cache::Cache<'a>,
    ) {
        let (screen_width, screen_height): (u32, u32) =
            gui.display.gl_window().window().inner_size().into();
        let text = self.get_text_ranged(range);
        let foreground_col: [u8; 3] = config
            .colors
            .get(&config.highlighting.foreground)
            .unwrap()
            .to_owned();
        draw_text(
            &text,
            (0, 0),
            //[171, 178, 191],
            foreground_col,
            config.font_size,
            true,
            gui,
            cache,
            target,
        );
    }
    pub fn get_text_ranged(&self, range: std::ops::Range<usize>) -> String {
        //let size = terminal_size().unwrap();
        let space = self.rope.len_lines().to_string().len() + 1;
        let mut end = range.end;
        if self.rope.len_lines() <= end {
            end = self.rope.len_lines();
        }
        let lines = self
            .rope
            .slice(self.rope.line_to_char(range.start)..self.rope.line_to_char(end))
            .to_string();
        lines
    }
    pub fn draw_status_line<'a>(
        &self,
        greyed: bool,
        gui_state: &mut crate::GuiState,
        target: &mut glium::Frame,
        cache: &mut rusttype::gpu_cache::Cache<'a>,
    ) {
        let (screen_width, screen_height): (f32, f32) =
            gui_state.display.gl_window().window().inner_size().into();
        let background_col: [u8; 3] = config
            .colors
            .get(&config.highlighting.background)
            .unwrap()
            .to_owned();
        let background_col = color_to_gl(background_col.into(), 255);
        let mut color = color_to_gl([76, 82, 99], 255);
        if greyed {
            color = color_to_gl([56, 62, 79], 255);
        }
        draw_rect(
            target,
            gui_state,
            screen_height - config.font_size - 44.,
            screen_height - config.font_size,
            0.,
            screen_width,
            color,
        );
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut temp_buffer: lyon::tessellation::VertexBuffers<lyon::math::Point, u16> =
            lyon::tessellation::VertexBuffers::new();
        let mut builder = lyon::tessellation::geometry_builder::simple_builder(&mut temp_buffer);
        if greyed {
            color = background_col;
        } else {
            match self.mode {
                Mode::Normal => color = color_to_gl([152, 195, 121], 255),
                Mode::Insert => color = color_to_gl([97, 175, 239], 255),
                _ => (),
            }
        }
        lyon::tessellation::basic_shapes::fill_circle(
            lyon::math::point(33., screen_height - 44.),
            8.,
            &lyon::tessellation::FillOptions::tolerance(0.05),
            &mut builder,
        )
        .unwrap();
        for vert in temp_buffer.vertices {
            vertices.push(Vertex {
                position: vert.to_array(),
                color,
            });
        }
        for index in temp_buffer.indices {
            indices.push(index);
        }
        use glium::uniform;
        let v_buf = glium::VertexBuffer::new(&gui_state.display, &vertices).unwrap();
        target
            .draw(
                &v_buf,
                &glium::index::IndexBuffer::new(
                    &gui_state.display,
                    glium::index::PrimitiveType::TrianglesList,
                    &indices,
                )
                .unwrap(),
                &gui_state.block_shader,
                &glium::uniform! {
                    resolution: [screen_width, screen_height],
                },
                &glium::DrawParameters {
                    blend: glium::Blend::alpha_blending(),
                    dithering: false,
                    ..Default::default()
                },
            )
            .unwrap();
        if !greyed {
            color = color_to_gl([86, 182, 194], 255);
        }
        draw_text(
            &self.cur_line().to_string(),
            (
                (screen_width - config.font_size * self.cur_line().to_string().len() as f32) as u32,
                (screen_height - config.font_size) as u32,
            ),
            [171, 178, 191],
            config.font_size,
            false,
            gui_state,
            cache,
            target,
        );
        draw_rect(
            target,
            gui_state,
            screen_height - config.font_size - 44.,
            screen_height - config.font_size,
            0.,
            5.,
            color,
        );
    }
    pub fn save(&self) {
        self.rope
            .write_to(BufWriter::new(
                std::fs::File::create(&self.filename).unwrap(),
            ))
            .unwrap();
    }
    pub fn newline(&mut self) {
        self.rope.insert(self.cur_y() as usize, "\n");
        self.x = 0;
        self.mode = Mode::Insert;
        self.down(1);
    }
    pub fn delete_line(&mut self) {
        let line_start = self.rope.line_to_char(self.cur_y() as usize);
        let line_end = self.rope.line_to_char(self.cur_y() as usize + 1);
        self.clipboard = format!("{}\n", self.rope.slice(line_start..line_end),);
        self.remove(line_start..line_end);
    }
    pub fn update_line_index(&mut self) {
        self.line_index.clear();
        let mut cur_line = 0;
        let mut view_line = 0;
        for line in self.rope.lines() {
            for _ in 0..(line.len_chars() / self.grid_width() as usize + 1) {
                self.line_index.insert(view_line, cur_line);
                view_line += 1;
            }
            cur_line += 1;
        }
    }
    pub fn paste(&mut self) {
        let text = self.clipboard.clone();

        if self.clipboard.contains("\n") {
            let line_idx = self.rope.line_to_char(self.cur_y() as usize);
            self.insert(line_idx, &text);
        } else {
            self.insert(
                self.rope.line_to_char(self.cur_y() as usize) + self.x as usize,
                &text,
            );
        }
    }
}

/*
fn draw_text_highlighted<'a>(
    text: &str,
    pos: (u32, u32),
    font_size: f32,
    wrapped: bool,
    gui: &mut crate::GuiState,
    cache: &mut crate::Cache<'a>,
    target: &mut glium::Frame,
) {
    let (screen_width, screen_height): (f32, f32) =
        gui.display.gl_window().window().inner_size().into();
    let glyphs = crate::get_glyphs(
        &gui.font,
        font_size,
        screen_width as u32 - pos.0,
        pos,
        &text,
        wrapped,
    );
    for glyph in &glyphs {
        cache.queue_glyph(0, glyph.clone());
    }
    cache
        .cache_queued(|rect, data| {
            gui.cache_tex.main_level().write(
                glium::Rect {
                    left: rect.min.x,
                    bottom: rect.min.y,
                    width: rect.width(),
                    height: rect.height(),
                },
                glium::texture::RawImage2d {
                    data: std::borrow::Cow::Borrowed(data),
                    width: rect.width(),
                    height: rect.height(),
                    format: glium::texture::ClientFormat::U8,
                },
            );
        })
        .unwrap();
    let uniforms = glium::uniform! {
        tex: gui.cache_tex.sampled().magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
    };
    let vertex_buffer = {
        let color = color_to_gl(color, 255);
        let (screen_width, screen_height) = {
            let (w, h) = gui.display.get_framebuffer_dimensions();
            (w as f32, h as f32)
        };
        let origin = point(0.0, 0.0);
        let vertices: Vec<TexVertex> = glyphs
            .iter()
            .filter_map(|g| cache.rect_for(0, g).ok().flatten())
            .enumerate()
            .flat_map(|num, (uv_rect, screen_rect)| {
                let gl_rect = Rect {
                    min: origin
                        + (vector(
                            screen_rect.min.x as f32 / screen_width - 0.5,
                            1.0 - screen_rect.min.y as f32 / screen_height - 0.5,
                        )) * 2.0,
                    max: origin
                        + (vector(
                            screen_rect.max.x as f32 / screen_width - 0.5,
                            1.0 - screen_rect.max.y as f32 / screen_height - 0.5,
                        )) * 2.0,
                };
                vec![
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.max.y],
                        tex_coords: [uv_rect.min.x, uv_rect.max.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.min.y],
                        tex_coords: [uv_rect.min.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.min.y],
                        tex_coords: [uv_rect.max.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.min.y],
                        tex_coords: [uv_rect.max.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.max.y],
                        tex_coords: [uv_rect.max.x, uv_rect.max.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.max.y],
                        tex_coords: [uv_rect.min.x, uv_rect.max.y],
                        color,
                    },
                ]
            })
            .collect();

        glium::VertexBuffer::new(&gui.display, &vertices).unwrap()
    };
    target
        .draw(
            &vertex_buffer,
            &glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList),
            &gui.text_shader,
            &uniforms,
            &glium::DrawParameters {
                blend: glium::Blend::alpha_blending(),
                ..Default::default()
            },
        )
        .unwrap();
}
*/

impl Program for Editor {
    fn draw(
        &mut self,
        greyed: bool,
        mut target: &mut glium::Frame,
        mut gui_state: &mut crate::GuiState,
        cache: &mut rusttype::gpu_cache::Cache,
    ) {
        if self.line_index.len() == 0 {
            self.screen_dimensions = gui_state.display.gl_window().window().inner_size().into();
            self.update_line_index();
        }
        let background_col: [u8; 3] = config
            .colors
            .get(&config.highlighting.background)
            .unwrap()
            .to_owned();
        let background_col = color_to_gl(background_col.into(), 255);
        target.clear_color_srgb(
            background_col[0],
            background_col[1],
            background_col[2],
            background_col[3],
        );
        let block_top = self.cursor_y as f32 * config.font_size;
        draw_rect(
            &mut target,
            &mut gui_state,
            block_top,
            block_top + config.font_size,
            0.,
            self.screen_dimensions.0 as f32,
            [0.0, 0.0, 0.0, 1.0],
        );
        self.draw_text(&mut gui_state, &mut target, cache);
        let mut block_right = 0.;
        let mut block_left = self.x as f32 * (config.font_size / 2.);
        let block_top = self.cursor_y as f32 * config.font_size;
        match self.mode {
            Mode::Normal => {
                block_right = block_left + config.font_size / 2.;
            }
            Mode::Insert => {
                block_left -= 1.;
                block_right = block_left + 2.;
            }
            _ => (),
        }
        self.draw_status_line(greyed, gui_state, target, cache);
        let block_bottom = block_top + config.font_size;
        let color = color_to_gl([86, 182, 194], 255);
        draw_rect(
            &mut target,
            &mut gui_state,
            block_top,
            block_bottom,
            block_left,
            block_right,
            color,
        );
        let mut command = String::new();
        for ch in &self.command {
            match ch {
                '\u{1b}' => command.push_str("ESC"),
                ' ' => command.push_str("SPC"),
                '\r' => command.push_str("RET"),
                _ => command.push(ch.to_owned()),
            }
            command.push('-');
        }
        draw_text(
            &command,
            (0, self.screen_dimensions.1 as u32 - config.font_size as u32),
            [171, 178, 191],
            config.font_size,
            false,
            &mut gui_state,
            cache,
            &mut target,
        );
        gui_state
            .display
            .gl_window()
            .window()
            .set_title(&(self.filename.clone() + " - S P A C E"));
    }
    fn handle_events(
        &mut self,
        event: glium::glutin::event::WindowEvent,
        gui_state: &mut crate::GuiState,
    ) -> Option<Box<dyn Program>> {
        use glium::glutin::event;
        let (screen_width, screen_height): (f32, f32) =
            gui_state.display.gl_window().window().inner_size().into();
        match event {
            event::WindowEvent::KeyboardInput { input, .. } => match input {
                event::KeyboardInput {
                    virtual_keycode: cod,
                    state: event::ElementState::Pressed,
                    ..
                } => {
                    if let Some(vkc) = cod {
                        match self.mode {
                            Mode::Normal => match vkc {
                                event::VirtualKeyCode::Down => {
                                    self.move_view(1);
                                }
                                _ => (),
                            },
                            Mode::Insert => match vkc {
                                event::VirtualKeyCode::Up => {
                                    self.up(1);
                                }
                                event::VirtualKeyCode::Down => {
                                    self.down(1);
                                }
                                event::VirtualKeyCode::Left => {
                                    self.left(1);
                                }
                                event::VirtualKeyCode::Right => {
                                    self.right(1);
                                }
                                _ => (),
                            },
                            _ => (),
                        }
                        gui_state.display.gl_window().window().request_redraw();
                    }
                }
                _ => (),
            },
            event::WindowEvent::ReceivedCharacter(ch) => {
                match self.mode {
                    Mode::Normal => {
                        if self.command.len() == 0 {
                            match ch {
                                'n' => self.left(1),
                                'e' => self.down(1),
                                'i' => self.up(1),
                                'o' => self.right(1),
                                'E' => self.move_view(10),
                                'I' => self.move_view(-10),
                                'u' => self.mode = Mode::Insert,
                                _ => {
                                    if ch.is_numeric() {
                                    } else {
                                        self.command.push(ch);
                                    }
                                }
                            }
                        } else {
                            if ch == '\u{8}' {
                                self.command.clear();
                            } else {
                                self.command.push(ch);
                            }
                        }
                    }
                    Mode::Insert => match ch {
                        '\r' => {
                            self.insert(self.get_current_pos(), "\n");
                            self.down(1);
                            self.x = 0;
                            self.update_line_index();
                        }
                        '\n' => (),
                        '\u{8}' => {
                            self.remove(self.get_current_pos() - 1..self.get_current_pos());
                            self.left(1);
                        }
                        '\u{1B}' => {
                            self.mode = Mode::Normal;
                        }
                        '\u{9}' => {
                            self.insert(self.get_current_pos(), "    ");
                            self.right(4);
                        }
                        '\u{7f}' => {
                            self.remove(self.get_current_pos()..self.get_current_pos() + 1);
                        }
                        _ => {
                            self.insert(self.get_current_pos(), &ch.to_string());
                            self.right(1);
                        }
                    },
                    _ => (),
                }
                let command: &[char] = &self.command;
                let mut clearable = true;
                match command {
                    &[' ', '\u{1B}'] => {
                        std::process::exit(0);
                    }
                    &[' ', 'b', 'w'] => {
                        self.save();
                    }
                    &[' ', 's'] => {
                        self.save();
                    }
                    &['d', 'd'] => {
                        self.delete_line();
                    }
                    &[' ', ' '] => {
                        return Some(Box::new(FileManager::new()));
                    }
                    &['p'] => {
                        self.paste();
                    }
                    _ => clearable = false,
                }
                if clearable {
                    self.command.clear();
                }
                gui_state.display.gl_window().window().request_redraw();
            }
            event::WindowEvent::Resized { .. } => {
                self.screen_dimensions = gui_state.display.gl_window().window().inner_size().into();
                self.update_line_index();
            }
            event::WindowEvent::MouseInput { state, button, .. } => match state {
                event::ElementState::Pressed => match button {
                    event::MouseButton::Left => (),
                    _ => (),
                },
                event::ElementState::Released => (),
            },
            _ => (),
        }
        None
    }
}
