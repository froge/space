pub fn open_file(path: std::path::PathBuf) -> Box<dyn super::Program> {
    if let Some(ext) = path.extension() {
        match ext.to_string_lossy().to_string().as_str() {
            #[cfg(feature = "music_player")]
            "ogg" => return Box::new(super::MusicPlayer::new(&path.to_string_lossy())),
            _ => (),
        }
    }
    Box::new(super::Editor::new(&path.to_string_lossy()))
}
