use super::Program;
use crate::{color_to_gl, config, draw_rect, draw_text, Vertex};
use glium::Surface;

pub struct StartScreen {
    cursor_pos: (u32, u32),
    command: Vec<char>,
    file: String,
}

impl StartScreen {
    pub fn new() -> Self {
        Self {
            cursor_pos: (0, 0),
            command: vec![],
            file: String::new(),
        }
    }
}
impl Program for StartScreen {
    fn draw(
        &mut self,
        greyed: bool,
        mut target: &mut glium::Frame,
        mut gui_state: &mut crate::GuiState,
        cache: &mut rusttype::gpu_cache::Cache,
    ) {
        let (screen_width, screen_height): (f32, f32) =
            gui_state.display.gl_window().window().inner_size().into();
        let background_col: [u8; 3] = config
            .colors
            .get(&config.highlighting.background)
            .unwrap()
            .to_owned();
        let background_col = color_to_gl(background_col.into(), 255);
        target.clear_color_srgb(
            background_col[0],
            background_col[1],
            background_col[2],
            background_col[3],
        );
        //state.draw_text(&mut gui_state, &mut target, &mut cache);
        //draw_text(&tempstring, ((screen_width / 2) - (screen_width / 9), 300), &mut gui_state, &mut cache, &mut target, 200.);
        let mut color = color_to_gl([76, 82, 99], 255);
        if greyed {
            color = color_to_gl([56, 62, 79], 255);
        }
        draw_rect(
            &mut target,
            &mut gui_state,
            screen_height - 66.,
            screen_height - 22.,
            0.,
            screen_width,
            color,
        );
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut temp_buffer: lyon::tessellation::VertexBuffers<lyon::math::Point, u16> =
            lyon::tessellation::VertexBuffers::new();
        let mut builder = lyon::tessellation::geometry_builder::simple_builder(&mut temp_buffer);
        if greyed {
            color = background_col;
        } else {
            color = color_to_gl([152, 195, 121], 255);
        }
        lyon::tessellation::basic_shapes::fill_circle(
            lyon::math::point(33., screen_height - 44.),
            7.,
            &lyon::tessellation::FillOptions::tolerance(0.05),
            &mut builder,
        )
        .unwrap();
        for vert in temp_buffer.vertices {
            vertices.push(Vertex {
                position: vert.to_array(),
                color,
            });
        }
        for index in temp_buffer.indices {
            indices.push(index);
        }
        use glium::uniform;
        let v_buf = glium::VertexBuffer::new(&gui_state.display, &vertices).unwrap();
        target
            .draw(
                &v_buf,
                &glium::index::IndexBuffer::new(
                    &gui_state.display,
                    glium::index::PrimitiveType::TrianglesList,
                    &indices,
                )
                .unwrap(),
                &gui_state.block_shader,
                &glium::uniform! {
                    resolution: [screen_width, screen_height],
                },
                &glium::DrawParameters {
                    blend: glium::Blend::alpha_blending(),
                    dithering: false,
                    ..Default::default()
                },
            )
            .unwrap();
        if !greyed {
            color = color_to_gl([86, 182, 194], 255);
        }
        draw_rect(
            &mut target,
            &mut gui_state,
            screen_height - 66.,
            screen_height - 22.,
            0.,
            5.,
            color,
        );
        /*
        let (block_left, block_top) =
            ((state.x as f32 * 12.), (state.cursor_y as f32 * 22.));
        let (block_right, block_bottom) = (
            (state.x as f32 * 12. + 12.),
            (state.cursor_y as f32 * 22. + 22.),
        );
        draw_rect(
            &mut target,
            &mut gui_state,
            block_top,
            block_bottom,
            block_left,
            block_right,
            color,
        );
        */
        let mut command = String::new();
        for ch in &self.command {
            match ch {
                ' ' => command.push_str("SPC"),
                _ => command.push(ch.to_owned()),
            }
            command.push('-');
        }
        draw_text(
            &command,
            (0, screen_height as u32 - 22),
            [171, 178, 191],
            config.font_size,
            false,
            gui_state,
            cache,
            &mut target,
        );
        draw_text(
            &self.file,
            (0, screen_height as u32 - 22),
            [171, 178, 191],
            config.font_size,
            false,
            gui_state,
            cache,
            &mut target,
        );
        gui_state
            .display
            .gl_window()
            .window()
            .set_title("start screen - S P A C E");
    }
    fn handle_events(
        &mut self,
        event: glium::glutin::event::WindowEvent,
        gui_state: &mut crate::GuiState,
    ) -> Option<Box<dyn Program>> {
        use glium::glutin::event;
        match event {
            event::WindowEvent::CursorMoved { position: pos, .. } => {
                self.cursor_pos = pos.into();
            }
            event::WindowEvent::ReceivedCharacter(ch) => {
                self.command.push(ch);
                let command: &[char] = &self.command;
                match command {
                    &[' ', ' '] => {
                        return Some(Box::new(crate::FileManager::new()));
                    }
                    _ => (),
                }
                gui_state.display.gl_window().window().request_redraw();
            }
            event::WindowEvent::MouseInput { state, button, .. } => match state {
                event::ElementState::Pressed => match button {
                    event::MouseButton::Left => (),
                    _ => (),
                },
                event::ElementState::Released => (),
            },
            _ => (),
        }
        None
    }
}
