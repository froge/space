use crate::{color_to_gl, config, draw_rect, draw_text, Vertex};
use glium::Surface;

/*
pub enum Input {
    Special(Key),
    Char(char),
}

enum Key {
    Up,
    Down,
    Left,
    Right,
    Backspace,
    Return,
    Tab,
    Escape,
    F(i32),
}
*/

pub trait Program {
    fn draw(
        &mut self,
        greyed: bool,
        target: &mut glium::Frame,
        gui_state: &mut crate::GuiState,
        cache: &mut rusttype::gpu_cache::Cache,
    );
    fn handle_events(
        &mut self,
        input: glium::glutin::event::WindowEvent,
        gui_state: &mut crate::GuiState,
    ) -> Option<Box<dyn Program>>;
}
