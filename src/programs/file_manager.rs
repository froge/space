use crate::{color_to_gl, config, draw_rect, draw_text, Program, Vertex};
use super::file_opener::open_file;
use glium::Surface;

enum Mode {
    Search,
    Edit,
}

pub struct FileManager {
    line: u32,
    search: String,
    mode: Mode,
    file: String,
    command: Vec<char>,
}

impl FileManager {
    pub fn new() -> Self {
        Self {
            line: 0,
            mode: Mode::Search,
            search: String::new(),
            file: String::new(),
            command: vec![],
        }
    }
}
impl Program for FileManager {
    fn draw(
        &mut self,
        greyed: bool,
        mut target: &mut glium::Frame,
        mut gui_state: &mut crate::GuiState,
        cache: &mut rusttype::gpu_cache::Cache,
    ) {
        let (screen_width, screen_height): (f32, f32) =
            gui_state.display.gl_window().window().inner_size().into();
        let background_col: [u8; 3] = config
            .colors
            .get(&config.highlighting.background)
            .unwrap()
            .to_owned();
        let background_col = color_to_gl(background_col.into(), 255);
        target.clear_color_srgb(
            background_col[0],
            background_col[1],
            background_col[2],
            background_col[3],
        );
        //state.draw_text(&mut gui_state, &mut target, &mut cache);
        //draw_text(&tempstring, ((screen_width / 2) - (screen_width / 9), 300), &mut gui_state, &mut cache, &mut target, 200.);
        let mut color = color_to_gl([76, 82, 99], 255);
        if greyed {
            color = color_to_gl([56, 62, 79], 255);
        }
        draw_rect(
            &mut target,
            &mut gui_state,
            screen_height - 66.,
            screen_height - 22.,
            0.,
            screen_width,
            color,
        );
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut temp_buffer: lyon::tessellation::VertexBuffers<lyon::math::Point, u16> =
            lyon::tessellation::VertexBuffers::new();
        let mut builder = lyon::tessellation::geometry_builder::simple_builder(&mut temp_buffer);
        if greyed {
            color = background_col;
        } else {
            color = color_to_gl([152, 195, 121], 255);
        }
        lyon::tessellation::basic_shapes::fill_circle(
            lyon::math::point(33., screen_height - 44.),
            7.,
            &lyon::tessellation::FillOptions::tolerance(0.05),
            &mut builder,
        )
        .unwrap();
        for vert in temp_buffer.vertices {
            vertices.push(Vertex {
                position: vert.to_array(),
                color: color,
            });
        }
        for index in temp_buffer.indices {
            indices.push(index);
        }
        use glium::uniform;
        let v_buf = glium::VertexBuffer::new(&gui_state.display, &vertices).unwrap();
        target
            .draw(
                &v_buf,
                &glium::index::IndexBuffer::new(
                    &gui_state.display,
                    glium::index::PrimitiveType::TrianglesList,
                    &indices,
                )
                .unwrap(),
                &gui_state.block_shader,
                &glium::uniform! {
                    resolution: [screen_width, screen_height],
                },
                &glium::DrawParameters {
                    blend: glium::Blend::alpha_blending(),
                    dithering: false,
                    ..Default::default()
                },
            )
            .unwrap();
        if !greyed {
            color = color_to_gl([86, 182, 194], 255);
        }
        draw_rect(
            &mut target,
            &mut gui_state,
            screen_height - 66.,
            screen_height - 22.,
            0.,
            5.,
            color,
        );
        match self.mode {
            Mode::Edit => {
                for (num, file) in std::env::current_dir()
                    .unwrap()
                    .read_dir()
                    .unwrap()
                    .enumerate()
                {
                    let file = file.unwrap();
                    if num == self.line as usize {
                        draw_rect(
                            &mut target,
                            &mut gui_state,
                            self.line as f32 * config.font_size,
                            self.line as f32 * config.font_size + config.font_size,
                            22.,
                            screen_width - 22.,
                            color_to_gl([97, 175, 239], 255),
                        );
                        self.file = file.file_name().to_str().unwrap().to_string();
                        draw_text(
                            file.file_name().to_str().unwrap(),
                            (22, num as u32 * config.font_size as u32),
                            [40, 44, 60],
                            config.font_size,
                            false,
                            gui_state,
                            cache,
                            target,
                        );
                    } else {
                        draw_text(
                            file.file_name().to_str().unwrap(),
                            (22, num as u32 * config.font_size as u32),
                            [171, 178, 191],
                            config.font_size,
                            false,
                            gui_state,
                            cache,
                            target,
                        );
                    }
                }
            }
            Mode::Search => {
                draw_text(
                    &format!("{}\n> {}", std::env::current_dir().unwrap().display(), self.search),
                    (0, 0),
                    [171, 178, 191],
                    config.font_size,
                    false,
                    gui_state,
                    cache,
                    target,
                );
                let mut strings = vec![];
                for (num, entry) in std::env::current_dir()
                    .unwrap()
                    .read_dir()
                    .unwrap()
                    .enumerate()
                {
                    let file = entry.unwrap().file_name();
                    let name = file.to_string_lossy().to_string();
                    if match_string(&self.search, &name) {
                        let ld = levenshtein_distance(&self.search, &name);
                        strings.push((ld, name));
                    }
                }
                draw_rect(
                    &mut target,
                    &mut gui_state,
                    config.font_size,
                    config.font_size + config.font_size,
                    (self.search.chars().count() as f32 + 2.) * (config.font_size / 2.),
                    (self.search.chars().count() as f32 + 2.) * (config.font_size / 2.) + 2.,
                    color_to_gl([97, 175, 239], 255),
                );
                strings.sort_by(|(ld1, _), (ld2, _)| ld1.partial_cmp(ld2).unwrap());
                for (num, tuple) in strings.iter().enumerate() {
                    let name = &tuple.1;
                    draw_text(
                        name,
                        (
                            (config.font_size) as u32,
                            ((num + 2) as f32 * config.font_size) as u32,
                        ),
                        [171, 178, 191],
                        config.font_size,
                        false,
                        gui_state,
                        cache,
                        target,
                    );
                }
            }
        }
        /*
        let (block_left, block_top) =
            ((state.x as f32 * 12.), (state.cursor_y as f32 * 22.));
        let (block_right, block_bottom) = (
            (state.x as f32 * 12. + 12.),
            (state.cursor_y as f32 * 22. + 22.),
        );
        draw_rect(
            &mut target,
            &mut gui_state,
            block_top,
            block_bottom,
            block_left,
            block_right,
            color,
        );
        */
        let mut command = String::new();
        for ch in &self.command {
            match ch {
                ' ' => command.push_str("SPC"),
                _ => command.push(ch.to_owned()),
            }
            command.push('-');
        }
        draw_text(
            &command,
            (0, screen_height as u32 - 22),
            [171, 178, 191],
            config.font_size,
            false,
            gui_state,
            cache,
            &mut target,
        );
        gui_state.display.gl_window().window().set_title(
            &(std::env::current_dir()
                .unwrap()
                .to_string_lossy()
                .to_string()
                + " - S P A C E"),
        );
    }
    fn handle_events(
        &mut self,
        event: glium::glutin::event::WindowEvent,
        gui_state: &mut crate::GuiState,
    ) -> Option<Box<dyn Program>> {
        use glium::glutin::event;
        match event {
            event::WindowEvent::KeyboardInput { input, .. } => match input {
                event::KeyboardInput {
                    virtual_keycode: cod,
                    state: event::ElementState::Pressed,
                    modifiers: mods,
                    ..
                } => {
                    match mods {
                        event::ModifiersState::CTRL => {
                            match cod.unwrap() {
                                event::VirtualKeyCode::Back => {
                                    if self.search.len() > 0 {
                                        while let Some(_) = self.search.pop() {
                                            if self.search.len() > 0 {
                                                if let Some(ch) = self.search.get(self.search.len() - 1..self.search.len()) {
                                                    if ch == " " {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                _ => (),
                            }
                        },
                        _ => (),
                    }
                }
                _ => (),
            },
            event::WindowEvent::ReceivedCharacter(ch) => {
                match self.mode {
                    Mode::Search => match ch {
                        '\u{9}' => {
                            let mut best_name = (100, "".to_string());
                            for entry in std::env::current_dir().unwrap().read_dir().unwrap() {
                                let file = entry.unwrap().file_name();
                                let name = file.to_string_lossy().to_string();
                                if match_string(&self.search, &name) {
                                    let ld = levenshtein_distance(&self.search, &name);
                                    if ld < best_name.0 {
                                        best_name.0 = ld;
                                        best_name.1 = name;
                                    }
                                }
                            }
                            let path = std::env::current_dir().unwrap().join(&best_name.1);
                            if path.exists() {
                                if path.is_dir() {
                                    std::env::set_current_dir(path).unwrap();
                                    self.search.clear();
                                } else {
                                    return Some(open_file(path));
                                }
                            } else {
                                return Some(open_file(path));
                            }
                        }
                        '\r' => {
                            let path = std::env::current_dir().unwrap().join(&self.search);
                            if path.exists() {
                                if path.is_dir() {
                                    std::env::set_current_dir(path).unwrap();
                                    self.search.clear();
                                } else {
                                    return Some(open_file(path));
                                }
                            } else {
                                return Some(open_file(path));
                            }
                        }
                        '\u{8}' => {
                            if self.search.len() == 0 {
                                std::env::set_current_dir(
                                    std::env::current_dir().unwrap().parent().unwrap(),
                                )
                                .unwrap();
                            } else {
                                self.search.pop();
                            }
                        }
                        _ => {
                            if !ch.is_control() {
                                self.search.push(ch)
                            }
                        },
                    },
                    Mode::Edit => match ch {
                        'n' => {
                            std::env::set_current_dir(
                                std::env::current_dir().unwrap().parent().unwrap(),
                            )
                            .unwrap();
                        }
                        'e' => self.line += 1,
                        'i' => self.line -= 1,
                        'o' => {
                            let path = std::env::current_dir().unwrap().join(&self.file);
                            if path.is_dir() {
                                std::env::set_current_dir(path).unwrap();
                            } else {
                                return Some(Box::new(crate::Editor::new(
                                    &path.display().to_string(),
                                )));
                            }
                        }
                        _ => (),
                    },
                }
                gui_state.display.gl_window().window().request_redraw();
            }
            _ => (),
        }
        None
    }
}

// Algorithm from wikipedia/rosettacode
fn levenshtein_distance(word1: &str, word2: &str) -> usize {
    let w1 = word1.chars().collect::<Vec<_>>();
    let w2 = word2.chars().collect::<Vec<_>>();

    let word1_length = w1.len() + 1;
    let word2_length = w2.len() + 1;

    let mut matrix = vec![vec![0; word1_length]; word2_length];

    for i in 1..word1_length {
        matrix[0][i] = i;
    }
    for j in 1..word2_length {
        matrix[j][0] = j;
    }

    for j in 1..word2_length {
        for i in 1..word1_length {
            let x: usize = if w1[i - 1] == w2[j - 1] {
                matrix[j - 1][i - 1]
            } else {
                1 + std::cmp::min(
                    std::cmp::min(matrix[j][i - 1], matrix[j - 1][i]),
                    matrix[j - 1][i - 1],
                )
            };
            matrix[j][i] = x;
        }
    }
    matrix[word2_length - 1][word1_length - 1]
}

fn match_string(word1: &str, word2: &str) -> bool {
    let mut lol: std::collections::VecDeque<char> = word1.chars().collect();
    //let mut chars = None;
    for ch in word2.chars() {
        if let Some(cmp) = lol.front() {
            if &ch == cmp {
                lol.pop_front();
            //if let Some(mut ch) = chars {
            //} else {
            //chars = Some(0);
            //}
            } else {
                if ch.is_uppercase() {
                    if ch == cmp.to_uppercase().next().unwrap() {
                        lol.pop_front();
                    }
                } else {
                    if ch == cmp.to_lowercase().next().unwrap() {
                        lol.pop_front();
                    }
                }
                //if let Some(mut ch) = chars {
                //chars = Some(ch + 1);
                //}
            }
        } else {
            break;
        }
    }
    //dbg!(chars, word2);
    if lol.len() == 0 {
        true
    } else {
        false
    }
}
