mod editor;
mod file_manager;
mod program;
mod start_screen;
mod term;
mod file_opener;
#[cfg(feature = "music_player")]
mod music_player;

pub use editor::*;
use file_opener::*;
pub use file_manager::*;
pub use program::*;
pub use start_screen::*;
pub use term::*;
#[cfg(feature = "music_player")]
use music_player::*;

enum CurrentStorage {
    #[cfg(feature = "music_player")]
    Music(rmus::Sink),
}
