#version 330 core

in vec2 position;
in vec4 color;

out vec4 v_color;

uniform vec2 resolution;

void main() {
    vec2 lol = position / resolution * 2 - 1;
    lol = vec2(lol.x, lol.y * - 1);
    gl_Position = vec4(lol, 0.0, 1.0);
    v_color = color;
}
