use glium::glutin;
use glium::Surface;
use glutin::event;
use lyon::path::builder::*;
use lyon::path::Path;
use lyon::tessellation::*;
use rusttype::gpu_cache::Cache;
use rusttype::{point, vector, Font, PositionedGlyph, Rect, Scale};
use std::fs::File;
use std::io::prelude::*;

mod programs;
use programs::*;

lazy_static::lazy_static! {
    static ref config: Config = {
        let mut tomlstr = String::new();
        let mut config_path = dirs::config_dir().unwrap().join("loledit");
        std::fs::create_dir_all(&config_path).unwrap();
        config_path = config_path.join("config.toml");
        if !config_path.exists() {}
        /*
        File::open(config_path)
            .unwrap()
            .read_to_string(&mut tomlstr)
            .unwrap();
        */
        tomlstr.push_str(include_str!("/home/robin/.config/loledit/config.toml"));
        toml::from_str(&tomlstr).expect("invalid config")
    };
}
#[derive(Debug, Copy, Clone)]
struct TexVertex {
    position: [f32; 2],
    tex_coords: [f32; 2],
    color: [f32; 4],
}

#[derive(Debug, Copy, Clone)]
struct Vertex {
    position: [f32; 2],
    color: [f32; 4],
}

pub struct GuiState {
    font: Font<'static>,
    display: glium::Display,
    text_shader: glium::Program,
    block_shader: glium::Program,
    cache_tex: glium::Texture2d,
    cursor_pos: (u32, u32),
    tempstring: String,
}

glium::implement_vertex!(Vertex, position, color);
glium::implement_vertex!(TexVertex, position, tex_coords, color);

fn main() {
    let mut space_menu = false;
    let mut current_program: Box<dyn Program> = Box::new(StartScreen::new());
    std::env::set_var("WINIT_UNIX_BACKEND", "x11");
    let mut event_loop = glutin::event_loop::EventLoop::new();

    let font_bytes = include_bytes!("/usr/share/fonts/TTF/HackNerdFontMono-Regular.ttf");
    let font = Font::try_from_bytes(font_bytes).unwrap();

    let mut tempstring = String::new();
    let mut cache = Cache::builder().dimensions(512, 512).build();

    let winbuild = glutin::window::WindowBuilder::new().with_title("S P A C E");
    let conbuild = glutin::ContextBuilder::new().with_srgb(true);
    let display = glium::Display::new(winbuild, conbuild, &event_loop).unwrap();

    let text_shader = glium::Program::new(
        &display,
        glium::program::ProgramCreationInput::SourceCode {
            vertex_shader: include_str!("text.vert"),
            fragment_shader: include_str!("text.frag"),
            outputs_srgb: true,
            geometry_shader: None,
            tessellation_control_shader: None,
            tessellation_evaluation_shader: None,
            transform_feedback_varyings: None,
            uses_point_size: false,
        },
    )
    .unwrap();
    let block_shader = glium::Program::new(
        &display,
        glium::program::ProgramCreationInput::SourceCode {
            vertex_shader: include_str!("block.vert"),
            fragment_shader: include_str!("block.frag"),
            outputs_srgb: true,
            geometry_shader: None,
            tessellation_control_shader: None,
            tessellation_evaluation_shader: None,
            transform_feedback_varyings: None,
            uses_point_size: false,
        },
    )
    .unwrap();

    let cache_tex = glium::texture::Texture2d::with_format(
        &display,
        glium::texture::RawImage2d {
            data: std::borrow::Cow::Owned(vec![128u8; 512 * 512]),
            width: 512,
            height: 512,
            format: glium::texture::ClientFormat::U8,
        },
        glium::texture::UncompressedFloatFormat::U8,
        glium::texture::MipmapsOption::NoMipmap,
    )
    .unwrap();
    let mut gui_state = GuiState {
        cache_tex,
        block_shader,
        font,
        display,
        text_shader,
        cursor_pos: (0, 0),
        tempstring,
    };
    event_loop.run(move |ev, _, control_flow| {
        *control_flow = glutin::event_loop::ControlFlow::Wait;
        match ev {
            event::Event::WindowEvent { event, .. } => match event {
                event::WindowEvent::CursorLeft { .. } => {
                    let mut target = gui_state.display.draw();
                    current_program.draw(true, &mut target, &mut gui_state, &mut cache);
                    target.finish().unwrap();
                }
                event::WindowEvent::CursorEntered { .. } => {
                    gui_state.display.gl_window().window().request_redraw();
                }
                event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit
                }
                event::WindowEvent::CursorMoved { position, .. } => {
                    gui_state.cursor_pos = position.into();
                    current_program.handle_events(event, &mut gui_state);
                }
                _ => {
                    if let Some(program) = current_program.handle_events(event, &mut gui_state) {
                        current_program = program;
                        gui_state.display.gl_window().window().request_redraw();
                    }
                }
            },
            event::Event::RedrawRequested(..) => {
                let mut target = gui_state.display.draw();
                current_program.draw(false, &mut target, &mut gui_state, &mut cache);
                target.finish().unwrap();
            }
            _ => (),
        }
    });
}

fn get_glyphs<'a>(
    font: &Font<'a>,
    scale: f32,
    width: u32,
    position: (u32, u32),
    text: &str,
    wrapped: bool,
) -> Vec<PositionedGlyph<'a>> {
    let mut glyphs = Vec::new();
    let scale = Scale::uniform(scale);
    let v_metrics = font.v_metrics(scale);
    let gap = v_metrics.ascent - v_metrics.descent;
    let gap = gap.round();
    let mut pos = point(
        0.0 + position.0 as f32,
        v_metrics.ascent + position.1 as f32,
    );
    let mut last_glyph_id = None;
    for c in text.chars() {
        if c.is_control() {
            match c {
                '\n' => {
                    pos = point(0.0, pos.y + gap);
                },
                '\u{9}' => {
                    pos = point(0.0, pos.y + gap * 4.);
                }

                _ => (),
            }
            continue;
        }
        let base_glyph = font.glyph(c);
        if let Some(id) = last_glyph_id.take() {
            pos.x += font.pair_kerning(scale, id, base_glyph.id());
        }
        last_glyph_id = Some(base_glyph.id());
        let mut glyph = base_glyph.scaled(scale).positioned(pos);
        if wrapped {
            if let Some(bb) = glyph.pixel_bounding_box() {
                if bb.max.x > width as i32 {
                    pos = point(0.0, pos.y + gap);
                    glyph.set_position(pos);
                    last_glyph_id = None;
                }
            }
        }
        pos.x += config.font_size / 2.;
        glyphs.push(glyph);
    }
    glyphs
}

fn draw_text<'a>(
    text: &str,
    pos: (u32, u32),
    color: [u8; 3],
    font_size: f32,
    wrapped: bool,
    gui: &mut GuiState,
    cache: &mut Cache<'a>,
    target: &mut glium::Frame,
) {
    let (screen_width, screen_height): (f32, f32) =
        gui.display.gl_window().window().inner_size().into();
    let glyphs = get_glyphs(
        &gui.font,
        font_size,
        screen_width as u32 - pos.0,
        pos,
        &text,
        wrapped,
    );
    for glyph in &glyphs {
        cache.queue_glyph(0, glyph.clone());
    }
    cache
        .cache_queued(|rect, data| {
            gui.cache_tex.main_level().write(
                glium::Rect {
                    left: rect.min.x,
                    bottom: rect.min.y,
                    width: rect.width(),
                    height: rect.height(),
                },
                glium::texture::RawImage2d {
                    data: std::borrow::Cow::Borrowed(data),
                    width: rect.width(),
                    height: rect.height(),
                    format: glium::texture::ClientFormat::U8,
                },
            );
        })
        .unwrap();
    let uniforms = glium::uniform! {
        tex: gui.cache_tex.sampled().magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
    };
    let vertex_buffer = {
        let color = color_to_gl(color, 255);
        let (screen_width, screen_height) = {
            let (w, h) = gui.display.get_framebuffer_dimensions();
            (w as f32, h as f32)
        };
        let origin = point(0.0, 0.0);
        let vertices: Vec<TexVertex> = glyphs
            .iter()
            .filter_map(|g| cache.rect_for(0, g).ok().flatten())
            .flat_map(|(uv_rect, screen_rect)| {
                let gl_rect = Rect {
                    min: origin
                        + (vector(
                            screen_rect.min.x as f32 / screen_width - 0.5,
                            1.0 - screen_rect.min.y as f32 / screen_height - 0.5,
                        )) * 2.0,
                    max: origin
                        + (vector(
                            screen_rect.max.x as f32 / screen_width - 0.5,
                            1.0 - screen_rect.max.y as f32 / screen_height - 0.5,
                        )) * 2.0,
                };
                vec![
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.max.y],
                        tex_coords: [uv_rect.min.x, uv_rect.max.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.min.y],
                        tex_coords: [uv_rect.min.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.min.y],
                        tex_coords: [uv_rect.max.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.min.y],
                        tex_coords: [uv_rect.max.x, uv_rect.min.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.max.x, gl_rect.max.y],
                        tex_coords: [uv_rect.max.x, uv_rect.max.y],
                        color,
                    },
                    TexVertex {
                        position: [gl_rect.min.x, gl_rect.max.y],
                        tex_coords: [uv_rect.min.x, uv_rect.max.y],
                        color,
                    },
                ]
            })
            .collect();

        glium::VertexBuffer::new(&gui.display, &vertices).unwrap()
    };
    target
        .draw(
            &vertex_buffer,
            &glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList),
            &gui.text_shader,
            &uniforms,
            &glium::DrawParameters {
                blend: glium::Blend::alpha_blending(),
                ..Default::default()
            },
        )
        .unwrap();
}

/*
fn gl_pos_from_pixel_pos(pixel_pos: (u32, u32), display: &glium::Display) -> [f32; 2] {
    let (screen_width, screen_height): (f32, f32) =
        display.gl_window().window().inner_size().into();
    let pixel_pos = (pixel_pos.0 as f32, pixel_pos.1 as f32);
    let x = ((pixel_pos.0 / screen_width) * 2.) - 1.;
    let y = ((pixel_pos.1 / screen_height) * 2.) - 1.;
    let y = y * -1.;
    [x, y]
}
*/

fn color_to_gl(color: [u8; 3], alpha: u8) -> [f32; 4] {
    [
        f32::from(color[0])/*.powf(1./2.2)*/ / 255.0,
        f32::from(color[1]) / 255.0,
        f32::from(color[2]) / 255.0,
        f32::from(alpha) / 255.0,
    ]
}
fn draw_rect(
    target: &mut glium::Frame,
    gui: &mut GuiState,
    top: f32,
    bottom: f32,
    left: f32,
    right: f32,
    color: [f32; 4],
) {
    use glium::uniform;
    let (screen_width, screen_height): (f32, f32) =
        gui.display.gl_window().window().inner_size().into();
    let vertices = [
        Vertex {
            position: [right, top],
            color,
        },
        Vertex {
            position: [right, bottom],
            color,
        },
        Vertex {
            position: [left, bottom],
            color,
        },
        Vertex {
            position: [left, top],
            color,
        },
    ];
    let v_buf = glium::VertexBuffer::new(&gui.display, &vertices).unwrap();
    target
        .draw(
            &v_buf,
            &glium::index::IndexBuffer::new(
                &gui.display,
                glium::index::PrimitiveType::TrianglesList,
                &[0u16, 1, 3, 1, 2, 3],
            )
            .unwrap(),
            &gui.block_shader,
            &glium::uniform! {
                resolution: [screen_width, screen_height],
            },
            &glium::DrawParameters {
                blend: glium::Blend::alpha_blending(),
                dithering: false,
                ..Default::default()
            },
        )
        .unwrap();
}
